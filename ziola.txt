sub zmien_nozyk
 finditem WSF C_ , %postac
 if #FINDID = x
 {
  finditem WSF C_ , %plecak
  if #FINDID = x
  {
   event sysmessage "Nie masz nozyka!"
   halt
  }
 }
 wait 1s
 set #LOBJECTID #FINDID
 set #LOBJECTTYPE #FINDTYPE
 set %nozykid #FINDID
 wait 1s
return


sub zbieraj
   set %nr ( %1 + 1 ) * 3 + ( %2 + 1 ) + 1
   event sysmessage Szukam ziol w miejscu %nr ( %1 %2 )s
   set #ltargetx #charposx + %1
   set #ltargety #charposy + %2
   event macro 17  //last object
   target
   event macro 22  //last target
   wait 1s
return

sub czekaj
   wait 2
   set %jEnd #jIndex
   event sysmessage Szukam od %1 do %jEnd
   if %1 <> %jEnd
   {
     for %i %1 %jEnd
     {
       set %current %i
       scanjournal %i
       if Twoje_narzedzie_do_niczego_juz_sie_nie_nadaje in #journal
       {
         gosub zmien_nozyk
         return -3
       }
       else
       {
         if Nie_udalo_Ci_sie_nic_znalezc in #journal
         {
           return -2
         }
         else
         {
           if Nie_dostrzegasz_tu_nic_nadajacego_sie_do_uzytku in #journal
           {
             return -4
           }
           else
           {
             if Znalazles_jakies_ziolo in #journal
             {
               return -1
             }
             else
             {
               if Wskazane_miejsce_jest_za_daleko in #journal
               {
                 return -4
               }
             }
           }
         }
       }
     }
   }
   return %current

sub zbierz_ziola
 set #LOBJECTID %nozykid
 set #LTARGETKIND 2
 for %x -1 1
 {
  for %y -1 1
  {
    set %tempres 0
    while %tempres <> -4
    {
      set %current #jIndex
      gosub zbieraj %x %y
      gosub czekaj %current
      set %tempres #result
      //event sysmessage Rezultat to %tempres
      while -1 < %tempres
      {
        if %tempres > 0
        {
          set %current %tempres
        }
        gosub czekaj %current
        set %tempres #result
        //event sysmessage Rezultat to %tempres
        if #enemyid <> N/A
        {
          set #LTARGETID #enemyid
          gosub zabij
        }
      }
    }
  }
 }
 return

sub zabij
  event macro 24 2 //arm/disarm right
  set #RHANDID %weapon
  while #ENEMYID <> N/A
  {
    set #LTARGETID #ENEMYID
    set #ltargetkind 1
    //event macro 15 11 //cast spell harm
    event macro 15 41 //cast energy bolt
    target
    event macro 22 //last target
  }
  event macro 15 24 //cast arch cure
  target
  event macro 23 0 //target self
  wait 1s
  event macro 15 28 //magic greater heal
  target
  event macro 23 0 //target self
  wait 1s
  event macro 24 2 //arm/disarm right
  set #LOBJECTID %nozykid
  return

set %plecak #BACKPACKID
set %postac #CHARID
set %weapon #LOBJECTID
gosub zmien_nozyk
for %k 0 7
{
  gosub zbierz_ziola
  event macro 5 %k //walk
  wait 1
  event macro 5 %k //walk
  wait 1
  event macro 5 %k //walk
  wait 1
  event macro 5 %k //walk
  wait 1
  event macro 5 %k //walk
  wait 1
}




